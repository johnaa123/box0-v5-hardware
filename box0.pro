update=Wed Jul 27 20:55:39 2016
version=1
last_client=kicad
[general]
version=1
[cvpcb]
version=1
NetIExt=net
[cvpcb/libraries]
EquName1=devcms
[pcbnew]
version=1
LastNetListRead=box0.net
UseCmpFile=1
PadDrill="    0.600000"
PadDrillOvalY="    0.600000"
PadSizeH="    1.500000"
PadSizeV="    1.500000"
PcbTextSizeV="    1.500000"
PcbTextSizeH="    1.500000"
PcbTextThickness="    0.300000"
ModuleTextSizeV="    1.000000"
ModuleTextSizeH="    1.000000"
ModuleTextSizeThickness="    0.150000"
SolderMaskClearance="    0.000000"
SolderMaskMinWidth="    0.000000"
DrawSegmentWidth="    0.200000"
BoardOutlineThickness="    0.100000"
ModuleOutlineThickness="    0.150000"
[pcbnew/libraries]
LibDir=
LibName1=sockets
LibName2=connect
LibName3=discret
LibName4=pin_array
LibName5=divers
LibName6=smd_capacitors
LibName7=smd_resistors
LibName8=smd_crystal&oscillator
LibName9=smd_dil
LibName10=smd_transistors
LibName11=libcms
LibName12=display
LibName13=led
LibName14=dip_sockets
LibName15=pga_sockets
LibName16=valves
LibName17=npth_drill
LibName18=conn_te_buchanan
LibName19=smd_inductors
LibName20=ti
LibName21=sot
[eeschema]
version=1
LibDir=lib/custom
[eeschema/libraries]
LibName1=STM32F072RxTx
LibName2=tlv274
LibName3=LD3985M3xR
LibName4=BAT54W
LibName5=tlv272
LibName6=lib/custom/REF303x
LibName7=conn
LibName8=power
LibName9=device
LibName10=lib/custom/LM2776
LibName11=lib/custom/logo_madresistor
LibName12=lib/custom/RT9728Bx
[schematic_editor]
version=1
PageLayoutDescrFile=
PlotDirectoryName=
SubpartIdSeparator=0
SubpartFirstId=65
NetFmtName=Pcbnew
SpiceForceRefPrefix=0
SpiceUseNetNumbers=0
LabSize=60
